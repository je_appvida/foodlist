'use strict'

var React = require('react-native');
var Main = require('./app/components/main');

var {
  AppRegistry
} = React;

AppRegistry.registerComponent('FoodList', () => Main);
