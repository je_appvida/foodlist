'use strict'

var React = require('react-native');
var ParallaxView = require('react-native-parallax-view');

var {
  StyleSheet,
  Component,
  View,
  Text,
  Image,
  AlertIOS,
  ListView,
  TouchableHighlight,
  ActivityIndicatorIOS
} = React;

var nutrient = [];

class DetailsView extends Component {
  constructor(props, food) {
    super(props, food);
    var dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
      sectionHeaderHasChanged: (sec1, sec2) => sec1 !== sec2
    });
    this.state = {
      isLoading: true,
      dataSource: dataSource.cloneWithRowsAndSections(this.convertNutrientArrayToMap())
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    var NDB_URL = 'https://api.nal.usda.gov/ndb/reports/?ndbno=' + this.props.food.ndbno + '&type=f&format=json&api_key=cFuZXGCEeOvxeLSgqvnXNVtJ09hY3x7x4iSnOd62';

     fetch(NDB_URL)
    .then((response) => response.json())
    .then((responseData) => {
      nutrient = responseData.report.food.nutrients;
      var dataSource = new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
        sectionHeaderHasChanged: (sec1, sec2) => sec1 !== sec2
      });
      this.setState({
        dataSource: dataSource.cloneWithRowsAndSections(this.convertNutrientArrayToMap(nutrient)),
        isLoading: false
      });
    })
    .done();
  }

  convertNutrientArrayToMap() {
    var nutrientCategoryMap = {}; //Create the blank map
    nutrient.forEach(function(foodItem){
      if(!nutrientCategoryMap[foodItem.group]) {
        //create an entry in the map for the category if if hasn't yet been created
        nutrientCategoryMap[foodItem.group] = [];
      }
      nutrientCategoryMap[foodItem.group].push(foodItem);
    });
    return nutrientCategoryMap;
  }

  render(){
    var imageURI = this.props.food.image;
    var description = (typeof this.props.food.notes !== 'undefined') ? this.props.food.notes : 'No description available.';
    return (
      <ParallaxView
          backgroundSource={{uri: imageURI}}
          windowHeight={300}
          header={
            <View style={styles.header}>
                <Text style={styles.headerText}>
                    {this.props.food.name}
                </Text>
            </View>
          }
      >
      <Text style={styles.asterik}>
        *Value per 100 g
      </Text>
      <View style={styles.separator} />
      <View style={styles.separator} />
      <ListView
      dataSource = {this.state.dataSource}
      renderRow = {this.renderFoodNutritionFacts.bind(this)}
      renderSectionHeader={this.renderSectionHeader.bind(this)}
      style = {styles.listView}
      />
      <Text style={styles.asterik}>
        *Percent Daily Values are based on a 2,000 calorie diet. Your daily values may be higher or lower depending on your calorie needs.
      </Text>
      </ParallaxView>
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.loading}>
        <ActivityIndicatorIOS size='large' />
        <Text>
          Loading Nutrition Facts...
        </Text>
      </View>
    );
  }

  renderFoodNutritionFacts(nutrients) {
       return (
              <View>
              <View style={styles.body}>
                <Text style={styles.leftText}>
                  {nutrients.name}
                </Text>
                <Text style={styles.rightText}>
                  {nutrients.value} {nutrients.unit}
                </Text>
              </View>
              <View style={styles.separator} />
              </View>
       );
   }

   renderSectionHeader(sectionData, group) {
     var sectionName = group;
     if (sectionName === 'Proximates') {
       sectionName = "General";
     } else if (sectionName === 'Lipids') {
       sectionName = "Fats";
     }
     return (
       <Text style={styles.sectionHeader}>
        {sectionName}
       </Text>
     )
   }
}

var styles = StyleSheet.create({
  header: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-end'
  },
  headerText: {
      fontSize: 40,
      fontWeight: 'bold',
      color: 'white',
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      padding: 10
  },
  body: {
      paddingHorizontal: 10,
      paddingVertical: 6,
      flexDirection: 'row'
  },
  leftText: {
      color: '#008080',
      fontSize: 14,
      fontWeight: 'bold',
      flex: 3
  },
  rightText: {
      color: '#717171',
      fontSize: 16,
      fontWeight: 'bold',
      alignSelf: 'flex-end',
      flex: 1
  },
  rightSubHeader: {
    color: '#717171',
    fontSize: 14,
    fontWeight: 'bold',
    alignSelf: 'flex-end',
    flex: 1,
    paddingHorizontal: 50,
    paddingVertical: 5
  },
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  asterik: {
    alignSelf: 'center',
    padding: 5,
    fontSize: 12
  },
  sectionHeader: {
    fontWeight: 'bold',
    fontSize: 16,
    padding: 5,
    backgroundColor: '#b9c1c3',
    color: 'white'
  }
});

module.exports = DetailsView;
