'use strict'

var React = require('react-native');
var STV = require('./scrollableTabView');
var Vegetables = require('./vegetables');
var Fruits = require('./fruits');
var Grains = require('./grains');
var Spices = require('./spices');
var DetailsView = require('./detailsView');

var ROUTES = {
    stv: STV,
    vegetables: Vegetables,
    fruits: Fruits,
    grains: Grains,
    spices: Spices,
    details: DetailsView
}

var {
  Component,
  StyleSheet,
  Navigator,
  View,
  Text,
  TouchableHighlight
} = React;

class Main extends Component {

  renderScene(route, navigator) {
    var Component = ROUTES[route.name];
    return (
      <Component route={route} navigator={navigator} {...route.passProps}/>
    )
  }

  render() {
    return (
        <Navigator
            style={styles.container}
            initialRoute={{
              name: 'stv',
              title: 'SEBI'
            }}
            renderScene={this.renderScene}
            configureScene={() => {return Navigator.SceneConfigs.FloatFromRight;}}
            navigationBar={
              <Navigator.NavigationBar
                style={styles.nav}
                routeMapper={NavigationBarRouteMapper} />}
        />
    );
  }

  onPress() {
  alert("YO FROM RIGHT BUTTON")
  }

  gotoNext() {
    this.props.navigator.push({
      component: Two,
      passProps: {
        id: 'MY ID',
      },
      onPress: this.onPress,
      rightText: 'ALERT!'
    })
  }

}

var NavigationBarRouteMapper = {
  LeftButton(route, navigator, index, navState) {
    if(index > 0) {
      return (
        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => { if (index > 0) { navigator.pop() } }}>
          <Text style={styles.leftNavButtonText}>Back</Text>
        </TouchableHighlight>)
    }
    else { return null }
  },
  RightButton(route, navigator, index, navState) {
    if (route.onPress) return (
      <TouchableHighlight
         onPress={ () => route.onPress() }>
         <Text style={styles.rightNavButtonText}>
              { route.rightText || 'Right Button' }
         </Text>
       </TouchableHighlight>)
  },
  Title(route, navigator, index, navState) {
    return <Text style={styles.title}>Alkaline Food</Text>
  }
};
var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navContainer: {
    flex: 4,
    flexDirection: 'column',
    marginTop: 64
  },
  leftNavButtonText: {
    fontSize: 18,
    marginLeft: 13,
    marginTop: 2,
    color: 'white'
  },
  rightNavButtonText: {
    fontSize: 18,
    marginRight: 13,
    marginTop: 2
  },
  nav: {
    height: 60,
    backgroundColor: '#008080'
  },
  title: {
    marginTop: 4,
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
  },
  button: {
    height: 60,
    marginBottom: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 18
  }
});

module.exports = Main;
