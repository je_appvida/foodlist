'use strict'

var React = require('react-native');
var ScrollableTabView = require('react-native-scrollable-tab-view');
var Vegetables = require('./vegetables');
var Fruits = require('./fruits');
var Grains = require('./grains');
var Spices = require('./spices');

var {
  Component,
  StyleSheet
} = React;

class STV extends Component {

  render() {
    return (
      <ScrollableTabView
      style={styles.container}
      tabBarUnderlineColor={'#008080'}
      tabBarActiveTextColor={'#008080'}
      tabBarInactiveTextColor={'gray'}
      tabBarPosition={'bottom'}
      tabBarBackgroundColor={'white'}
      >
        <Vegetables tabLabel="Vegetables" navigator={this.props.navigator} />
        <Fruits tabLabel="Fruits" navigator={this.props.navigator} />
        <Grains tabLabel="Grains" navigator={this.props.navigator} />
        <Spices tabLabel="Spices" navigator={this.props.navigator} />
      </ScrollableTabView>
    );
  }

}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 20
  }
});

module.exports = STV;
