'use strict'

var React = require('react-native');
var DetailsView = require('./detailsView');

//Define routes
var ROUTES = {
  detailsView: DetailsView
}

var {
  Component,
  StyleSheet,
  View,
  Text,
  ListView,
  Image,
  TouchableHighlight,
  Alert
} = React;

// var REQUEST_URL = 'https://spreadsheets.google.com/feeds/list/1W1qoB2DkurmAULgRZeRN0Ez8xAQ1hZXMpFxxtK7w6Xk/od6/public/basic?alt=json';
var JSON_URL = 'https://api.myjson.com/bins/2no8y';

class Vegetables extends Component {
  constructor(props) {
    super(props);
    this.showFoodDetail = this.showFoodDetail.bind(this);
    this.state = {
      isLoading: true,
      dataSource: new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2
      })
    };
  }
    componentDidMount() {
      this.fetchData();
    }

    fetchData() {
      fetch(JSON_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.data),
        });
      })
      .done();
    }

    render() {
      return (
        <ListView
          dataSource = {this.state.dataSource}
          renderRow = {this.renderFoodList.bind(this)}
          style = {styles.listView}
          />
      );
    }

    renderFoodList(food) {
      return (
        <TouchableHighlight
          onPress={() => this.showFoodDetail(food)}
          underlayColor='#dddddd'
          >
            <View>
              <View style={styles.container}>
                <View style={styles.rightContainer}>
                  <Image
                    source={{uri: food.image}}
                    style={styles.thumbnail} >
                    <Text style={styles.phScore}>
                      {food.phLevel}
                    </Text>
                    </Image>
                  <Text style={styles.name}>
                    {food.name}
                  </Text>
                  <Text style={styles.type}>
                    {food.type}
                  </Text>
                </View>
              </View>
              <View style={styles.separator} />
            </View>
        </TouchableHighlight>
      );
    }

    showFoodDetail = (food) => {
      if (food.ndbno != "") {
          this.props.navigator.push({
            name: 'details',
            title: food.name,
            passProps: {
              food: food
            }
        });
      } else {
        alert(
          'Sorry, but the nutritional data on this food is currently not available.',
          [
            {text: 'OK', onPress: () => console.log('OK button pressed')},
          ]
        )
      }
    }

  }

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f9f9f9',
    padding: 10
  },
  separator: {
    height: 1,
    backgroundColor: '#dddddd'
  },
  listView: {
    backgroundColor: '#F5FCFF',
    marginTop: 60
  },
  thumbnail: {
    width: 300,
    height: 300,
    borderRadius: 7,
    backgroundColor: '#ffffff',
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  name: {
    fontSize: 20,
    color: '#008080'
  },
  type: {
    color: 'gray'
  },
  phScore: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0, 128, 128, 0.5)',
    borderRadius: 6,
    alignSelf: 'flex-end',
    margin: 10,
    padding: 10
  }
});

module.exports = Vegetables;
